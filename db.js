"user strict";

var mysql = require("mysql");

//local mysql db connection
var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "admin",
  database: "trafficfine"
});

// var connection = mysql.createConnection({
//   host: "https://remotemysql.com/",
//   user: "8ppHq33OiG",
//   password: "RRcfM@SGDQ@TVB7",
//   database: "8ppHq33OiG"
// });

connection.connect(function(err) {
  if (!err) {
    console.log("Database connection succeeded...!");
  } else {
    console.log("Error in DB connection :" + JSON.stringify(err, undefined, 2));
  }
});

module.exports = connection;

// module.exports = {
//     "secret": "myapplicationsecret"
// };
