const express = require("express");
var router = express.Router();

var jwt = require("jsonwebtoken");
const secret = "jobmextuv2345";
const FabricCAServices = require("fabric-ca-client");
const {
  FileSystemWallet,
  X509WalletMixin,
  Gateway
} = require("fabric-network");
const fs = require("fs");
const path = require("path");

var User = require("../models/user");
const passport = require("passport");

const { check, validationResult } = require("express-validator/check");

const ccpPath = path.resolve(
  __dirname,
  "..",
  "tmp",
  "client",
  "Generalpublic",
  "generalpublic-trafficfine-network.json"
); // The file path to network configeration file
const ccpJSON = fs.readFileSync(ccpPath, "utf8"); // read the file
const ccp = JSON.parse(ccpJSON); // convert it into a javscript object

router.post(
  "/register",
  [
    check("identity")
      .exists()
      .isString()
      .withMessage("Must be a string"),
    check("first_name")
      .exists()
      .isString()
      .isAlpha()
      .withMessage("Must be alphabetical Chars"),
    check("last_name")
      .exists()
      .isString()
      .isAlpha()
      .withMessage("Must be alphabetical Chars"),
    check("nic")
      .exists()
      .isString()
      .isAlphanumeric()
      .withMessage("Must be alphabeticl Chars and Numbers"),
    check("licenceNo")
      .exists()
      .isString()
      .isAlphanumeric()
      .withMessage("Must be alphabeticl Chars and Numbers"),
    check("username")
      .exists()
      .isString()
      .withMessage("Must be a string"),
    check("password").exists()
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      console.log(req.body.username)
      User.get_user_by_username(
        req.body.username,
        async (err, user, result) => {
          if (Object.keys(user).length == 0) {
            try {
              const caInfo =
                ccp.certificateAuthorities["ca.generalpublic.trafficfine.com"];
              const caTLSCACerts = caInfo.tlsCACerts;
              const ca = new FabricCAServices(
                caInfo.url,
                { trustedRoots: caTLSCACerts.cacert, verify: false },
                caInfo.caName
              ); //creating the acces to ca cerver

              // Create a new file system based wallet for managing identities.
              const walletPath = path.join(process.cwd(), "wallet");
              const wallet = new FileSystemWallet(walletPath);
              console.log(`Wallet path: ${walletPath}`);

              const userExists = await wallet.exists(req.body.identity);

              if (userExists) {
                console.log(
                  `An identity for the driver ${req.body.identity} already exists in the wallet`
                );
                res.status(409).json({
                  state: false,
                  msg: {
                    type: "error",
                    content:
                      "Identity for Driver is already exists in the wallet"
                  }
                });
              } else {
                const adminExists = await wallet.exists("admin");
                console.log(adminExists);

                if (adminExists) {
                  const gateway = new Gateway();
                  await gateway.connect(ccpPath, {
                    wallet,
                    identity: "admin",
                    discovery: { enabled: true, asLocalhost: true }
                  });

                  // Get the CA client object from the gateway for interacting with the CA.
                  const ca = gateway.getClient().getCertificateAuthority();
                  const adminIdentity = gateway.getCurrentIdentity();

                  // Register the user, enroll the user, and import the new identity into the wallet.
                  const secret = await ca.register(
                    {
                      affiliation: "org1.department1",
                      enrollmentID: req.body.identity,
                      role: "client"
                    },
                    adminIdentity
                  );
                  const enrollment = await ca.enroll({
                    enrollmentID: req.body.identity,
                    enrollmentSecret: secret
                  });
                  const userIdentity = X509WalletMixin.createIdentity(
                    "GeneralpublicMSP",
                    enrollment.certificate,
                    enrollment.key.toBytes()
                  );
                  await wallet.import(req.body.identity, userIdentity);

                  if (enrollment) {
                    var driver = {
                      firstname: req.body.first_name,
                      lastname: req.body.last_name,
                      user_identity: req.body.identity,
                      identity_key: req.body.licenceNo
                    };

                    var user = {
                      username: req.body.username,
                      password: req.body.password,
                      type: "driver",
                      identity_key: req.body.licenceNo
                    };
                    User.save_user(user, async (err, user) => {
                      if (!err) {
                        console.log("Succesfully Added");
                      }
                    });

                    User.save_driver(driver, async (err, user) => {
                      if (err) {
                        throw err;
                      }

                      if (user) {
                        try {
                          const walletPath = path.join(process.cwd(), "wallet");
                          const wallet = new FileSystemWallet(walletPath);
                          const userExists = await wallet.exists(
                            req.body.identity
                          );
                          console.log(userExists);
                          if (!userExists) {
                            res.status(409).json({
                              state: false,
                              msg: `An identity for the user ${user_sel[0].user_identity}  does not exist in the wallet`
                            });
                          } else {
                            const gateway = new Gateway(); //Creating the gateway to acces the network
                            await gateway.connect(ccpPath, {
                              wallet,
                              identity: req.body.identity,
                              discovery: {
                                enabled: true,
                                asLocalhost: true
                              }
                            });

                            const network = await gateway.getNetwork(
                              "trafficfine"
                            );

                            // Get the contract from the network.
                            const contract = network.getContract("trafficfine");

                            // Submit the specified transaction.
                            await contract.submitTransaction(
                              "driverRegistration",
                              req.body.nic,
                              req.body.first_name,
                              req.body.last_name,
                              req.body.licenceNo
                            );

                            await contract.submitTransaction(
                              "verifyDriver",
                              req.body.licenceNo,
                              "943234087V"
                            );
                            const driver_sel = await contract.submitTransaction(
                              "selectNthDriver",
                              req.body.licenceNo
                            );
                            res.status(200).json({
                              state: true,
                              msg: driver_sel.toString()
                            });
                          }
                        } catch (error) {
                          res.status(400).json({ status: false, msg: error });
                        }
                      } else {
                        res.status(400).json({
                          state: false,
                          msg: "Something went wrong"
                        });
                      }
                    });
                  } else {
                    res.status(400).json({
                      state: false,
                      msg: "Something went wrong"
                    });
                  }
                } else {
                  res.status(400).json({
                    state: false,
                    msg: "Something went wrong"
                  });
                }
              }
            } catch (err) {
              res.status(400).json({ status: false, msg: err });
            }
          } else {
            res.status(400).json({
              state: false,
              msg: "Something went wrong"
            });
          }
        }
      );
    }
  }
);

router.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.pass;

  User.get_user_by_username(username, async (err, user, result) => {
    //check wether user exists
    if (err) {
      throw err;
    }

    if (!user) {
      res.status(203).json({ state: false, msg: "No user found" });
    }

    if (user) {
      try {
        User.passwordCheck(password, user[0].password, function(err, match) {
          //check wether password is matching or not
          if (err) {
            res
              .status(406)
              .json({ state: false, msg: "your password is incorrect" });
          }

          const user_sel = {
            user_id: user[0].user_id,
            username: user[0].username,
            type: user[0].type,
            identity_key: user[0].identity_key
          };
          if (match) {
            const token = jwt.sign(user_sel, secret, {
              //create jwt token for the authorized user
              expiresIn: 86400 * 3
            });
            res.json({
              state: true,
              token: token,
              user: user_sel
            });
          } else {
            res
              .status(401)
              .json({ state: false, msg: "password does not match" });
          }
        });
      } catch (error) {
        res.status(400).json({
          status: false,
          msg: error
        });
      }
    }
  });
});

router.get(
  "/getNthDriverUserDetails/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.getNthDriverUserDetails(req.params.id, (err, user, result) => {
      if (err) {
        throw err;
      }

      if (user) {
        res.status(200).json({
          status: true,
          user: user
        });
      } else {
        res.status(400).json({
          status: false,
          msg: "Something Went wrong"
        });
      }
    });
  }
);

router.get(
  "/getNthDriverDetails/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.getNthDriverDetails(req.params.id, (err, user, result) => {
      if (err) {
        throw err;
      }

      if (user) {
        res.status(200).json({
          status: true,
          user: user
        });
      } else {
        res.status(400).json({
          status: false,
          msg: "Something Went wrong"
        });
      }
    });
  }
);

router.get(
  "/getNthDriverDetailsByLicenceNo/:licenceNo",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.getNthDriverDetails(
      req.params.licenceNo,
      async (err, user, result) => {
        if (err) {
          throw err;
        }

        if (user) {
          if (Object.keys(user).length != 0) {
            console.log(user);
            try {
              // Create a new file system based wallet for managing identities.
              const walletPath = path.join(process.cwd(), "wallet");
              const wallet = new FileSystemWallet(walletPath);
              console.log(`Wallet path: ${walletPath}`);

              // Check to see if we've already enrolled the user.
              const driverExists = await wallet.exists(user[0].user_identity);
              if (!driverExists) {
                res.status(400).json({
                  state: false,
                  msg: `An identity for the user ${user[0].user_identity}  does not exist in the wallet`
                });
              } else {
                const gateway = new Gateway(); //Creating the gateway to acces the network
                await gateway.connect(ccpPath, {
                  wallet,
                  identity: user[0].user_identity,
                  discovery: {
                    enabled: true,
                    asLocalhost: true
                  }
                });

                const network = await gateway.getNetwork("trafficfine");

                // Get the contract from the network.
                const contract = network.getContract("trafficfine");

                const driver_sel = await contract.submitTransaction(
                  "selectNthDriver",
                  req.params.licenceNo
                );

                const organizedDriver_sel = JSON.parse(driver_sel.toString());

                res.status(200).json({
                  state: true,
                  msg: organizedDriver_sel
                });
              }
            } catch (err) {
              res.status(400).json({
                state: false,
                msg: `Failed to get driver ${req.params.licenceNo}: ${err}`
              });
            }
          } else {
            res.status(400).json({
              state: false,
              msg: `Failed to get driver ${req.params.licenceNo}`
            });
          }
        }
      }
    );
  }
);

router.post(
  "/changepassword",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const user_id = req.body.user;
    const password = req.body.password;
    const prvPassword = req.body.prvPassword;

    console.log(user_id);
    console.log(password);
    console.log(prvPassword);
    User.findUserbyId(user_id, (err, user, result) => {
      if (err) {
        throw err;
      }

      if (user) {
        try {
          User.passwordCheck(prvPassword, user[0].password, function(
            err,
            match
          ) {
            //check wether password is matching or not
            if (err) {
              res
                .status(406)
                .json({ state: false, msg: "your prv Password is incorrect" });
            }

            if (match) {
              User.change_password(password, user_id, (err, user) => {
                if (err) {
                  throw err;
                }

                if (user) {
                  res
                    .status(200)
                    .json({ state: false, msg: "Successfully Updated" });
                }
              });
            } else {
              res
                .status(400)
                .json({ state: false, msg: "password does not match" });
            }
          });
        } catch (error) {
          res.status(400).json({
            status: false,
            msg: error
          });
        }
      } else {
        res.status(400).json({
          state: false,
          msg: "No user found"
        });
      }
    });
  }
);

module.exports = router;

module.exports.setupFunction = async () => {
  const caInfo = ccp.certificateAuthorities["ca.generalpublic.trafficfine.com"];
  const caTLSCACerts = caInfo.tlsCACerts;
  const ca = new FabricCAServices(
    caInfo.url,
    { trustedRoots: caTLSCACerts.cacert, verify: false },
    caInfo.caName
  ); //creating the acces to ca cerver

  // Create a new file system based wallet for managing identities.
  const walletPath = path.join(process.cwd(), "wallet");
  const wallet = new FileSystemWallet(walletPath);
  console.log(`Wallet path: ${walletPath}`);

  const adminExists = await wallet.exists("admin");
  if (!adminExists) {
    const enrollment = await ca.enroll({
      enrollmentID: "admin",
      enrollmentSecret: "adminpw"
    }); //enroll  the admin
    if (enrollment) {
      const identity = X509WalletMixin.createIdentity(
        "GeneralpublicMSP",
        enrollment.certificate,
        enrollment.key.toBytes()
      ); //create the identity
      await wallet.import("admin", identity);
    } else {
      console.log("enrollment failed");
    }
  }
};
