const express = require("express");
var router = express.Router();

var jwt = require("jsonwebtoken");
const secret = "jobmextuv2345";
const FabricCAServices = require("fabric-ca-client");
const {
  FileSystemWallet,
  X509WalletMixin,
  Gateway
} = require("fabric-network");
const fs = require("fs");
const path = require("path");
var User = require("../models/user");
const passport = require("passport");

const { check, validationResult } = require("express-validator/check");

const ccpPath = path.resolve(
  __dirname,
  "..",
  "tmp",
  "client",
  "Generalpublic",
  "generalpublic-trafficfine-network.json"
); // The file path to network configeration file
const ccpJSON = fs.readFileSync(ccpPath, "utf8"); // read the file
const ccp = JSON.parse(ccpJSON); // convert it into a javscript object

router.get(
  "/nthoffence/:id/:licenceNo",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    console.log(req.params.licenceNo);
    console.log(req.params.id);
    User.getNthDriverDetails(
      req.params.licenceNo,
      async (err, user, result) => {
        if (err) {
          throw err;
        }

        if (user) {
          if (Object.keys(user).length != 0) {
            console.log(user);
            try {
              // Create a new file system based wallet for managing identities.
              const walletPath = path.join(process.cwd(), "wallet");
              const wallet = new FileSystemWallet(walletPath);
              console.log(`Wallet path: ${walletPath}`);

              // Check to see if we've already enrolled the user.
              const driverExists = await wallet.exists(user[0].user_identity);
              if (!driverExists) {
                res.status(400).json({
                  state: false,
                  msg: `An identity for the user ${user[0].user_identity}  does not exist in the wallet`
                });
              } else {
                const gateway = new Gateway(); //Creating the gateway to acces the network
                await gateway.connect(ccpPath, {
                  wallet,
                  identity: user[0].user_identity,
                  discovery: {
                    enabled: true,
                    asLocalhost: true
                  }
                });

                const network = await gateway.getNetwork("trafficfine");

                // Get the contract from the network.
                const contract = network.getContract("trafficfine");

                const driver_sel = await contract.submitTransaction(
                  "selectNthOffence",
                  req.params.id
                );

                const organizedDriver_sel = JSON.parse(driver_sel.toString());

                res.status(200).json({
                  state: true,
                  msg: organizedDriver_sel
                });
              }
            } catch (err) {
              res.status(400).json({
                state: false,
                msg: `Failed to get driver ${req.params.licenceNo}: ${err}`
              });
            }
          } else {
            res.status(400).json({
              state: false,
              msg: `Failed to get driver ${req.params.licenceNo}`
            });
          }
        }
      }
    );
  }
);

module.exports = router;
