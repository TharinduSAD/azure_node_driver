const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const passport = require("passport");
require("./config/passport")(passport);

var user_controller = require("./controllers/user_controller");
var fine_controller = require("./controllers/fine_controller");
var offence_controller = require("./controllers/offence_controller");

const path = require("path");

var app = express();
app.use(helmet());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

user_controller.setupFunction();

app.listen(3002, function() {
  console.log("Server started at port : 3002");
});

app.use("/user", user_controller);
app.use("/fine", fine_controller);
app.use("/offence", offence_controller)
