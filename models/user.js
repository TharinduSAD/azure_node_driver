var bcrypt = require("bcryptjs");
var sql = require("../db.js");

module.exports.get_user_by_username = (username, callback) => {
  sql.query("SELECT * FROM users WHERE username='" + username + "'", callback);
};

//
module.exports.passwordCheck = (plainpassword, hash, callback) => {
  bcrypt.compare(plainpassword, hash, (err, res) => {
    if (err) {
      throw err;
    } else {
      callback(null, res);
    }
  });
};

module.exports.change_password = (password, user_id, callback) => {
  bcrypt.hash(password, 10, (err, hash) => {
    password = hash;
    if (err) {
      throw err;
    } else {
      console.log(password);

      sql.query(
        "UPDATE users SET password = '" +
          password +
          "' WHERE user_id = '" +
          user_id +
          "'",
        callback
      );
    }
  });
};

module.exports.save_user = (user, callback) => {
  bcrypt.hash(user.password, 10, (err, hash) => {
    user.password = hash;
    if (err) {
      throw err;
    } else {
      console.log(user.password);

      sql.query("INSERT INTO users set ?", user, callback);
    }
  });
};

module.exports.save_driver = (newUser, callback) => {
  sql.query("INSERT INTO drivers set ?", newUser, callback);
};

module.exports.getNthDriverUserDetails = (id, callback) => {
  sql.query("SELECT * FROM users WHERE user_id='" + id + "'", callback);
};

module.exports.getNthDriverDetails = (id, callback) => {
  sql.query("SELECT * FROM drivers WHERE identity_key='" + id + "'", callback);
};

module.exports.findUserbyId = (id, callback) => {
  sql.query("SELECT * FROM users WHERE user_id ='" + id + "'", callback);
};

module.exports.findSuperUser = (identityKey, callback) => {
  sql.query(
    "SELECT * FROM administrators WHERE identity_key ='" +
      identityKey +
      "' AND admin_level = 'Super'",
    callback
  );
};

module.exports.finduserByIdentityKey = (identityKey, callback) => {
  sql.query(
    "SELECT * FROM administrators WHERE identity_key ='" + identityKey + "'",
    callback
  );
};

module.exports.findOfficerByIdentityKey = (identityKey, callback) => {
  sql.query("SELECT * FROM administrators WHERE user_id = 1", callback);
};

module.exports.getAllAdministrators = callback => {
  sql.query("SELECT * FROM administrators", callback);
};

module.exports.get_admin_by_identitykey = (identityKey, callback) => {
  sql.query(
    "SELECT * FROM administrators WHERE identity_key ='" + identityKey + "'",
    callback
  );
};

module.exports.get_officer_by_identity_key = (identityKey, callback) => {
  sql.query(
    "SELECT * FROM users WHERE identity_key ='" + identityKey + "'",
    callback
  );
};
